package main

import (
	// Text
	// "golang.org/x/image/font/inconsolata" // Panics for some reason, don't care
	pxl "github.com/faiface/pixel"
	"golang.org/x/image/font/basicfont"

	"github.com/faiface/pixel/text"
)

var (
	FontAtlas *text.Atlas = text.NewAtlas(
		basicfont.Face7x13, text.ASCII)
)

func NewText(orig pxl.Vec) *text.Text {
	return text.New(orig, FontAtlas)
}
