package main

import (
	pxl "github.com/faiface/pixel"
)

// Cursor data updated at every frame
var CursorLast, CursorNow pxl.Vec

// The board world object
var TheBoard Board

// Selecting the controlled piece
var PieceSelected *Piece

func Update(dt SecsDT) {
	UpdateAnims(dt)
	updateCursor()
	mouseHandler()
}

func updateCursor() {
	CursorLast = CursorNow
	CursorNow = Window.MousePosition()
}

func HoveredPiece() *Piece {
	for piece, hitbox := range PieceHitboxes {
		Assert(piece != nil)
		if hitbox.Contains(CursorNow) {
			return piece
		}
	}
	return nil
}

func HoveredTile() (Pos, bool) {
	for tile, hitbox := range TileHitboxes {
		Assert(isOnBoard(tile))
		if hitbox.Contains(CursorNow) {
			return tile, true
		}
	}
	return Pos{}, false
}

func mouseHandler() {
	var justClicked = Window.JustPressed(KEY_LCLICK)

	hoveredPiece := HoveredPiece()
	PiecePreviewed = hoveredPiece
	{ // Piece (un)selection
		if hoveredPiece != nil {
			var hasTurn = hoveredPiece.Loyalty == TheBoard.CurrentTurn
			if justClicked && hasTurn {
				if PieceSelected != hoveredPiece {
					PieceSelected = hoveredPiece
				} else {
					PieceSelected = nil
				}
				return
			}
		}
	}
	if PieceSelected == nil {
		return
	}

	var tileClicked Pos
	if justClicked {

		defer func() { PieceSelected = nil }()

		tileHovered, isHoveringOverTile := HoveredTile()
		if !isHoveringOverTile {
			return
		}
		tileClicked = tileHovered
	}

	{ // Test: Can selected piece move on clicked tile?
		var moveValid = false
		for _, pos := range ValidMovesFor(*PieceSelected) {
			if pos == tileClicked {
				moveValid = true
			}
		}
		if !moveValid {
			return
		}
	}

	// Before we move...
	pieceCaptured, isMoveCapture := PieceAt(tileClicked)
	// canPromote := PromoPossible(PieceSelected.TilePosition, tileClicked)

	DoMove(PieceSelected, tileClicked)

	StartTrans_Vec(
		&PieceSelected.DrawPos, Dur_PieceMove, PieceSelected.TilePosition.Vec())

	if isMoveCapture {
		CapturePiece_gfx(pieceCaptured)
		RenderAtTop(pieceCaptured)
	}
	RenderAtTop(PieceSelected)

	EndTurn()
	EndTurn_gfx()

	Prt("\nTURN %d\n%s\n\n", TheBoard.Turn, TheBoard.String())

}
