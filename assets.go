package main

import (
	"os"
	"path/filepath"
	"strings"

	// Images
	pxl "github.com/faiface/pixel"
	"image"
	_ "image/png"

	// Sounds
	"github.com/faiface/beep"
	"github.com/faiface/beep/speaker"
	wav "github.com/faiface/beep/wav"
	"math/rand"
	"time"
)

var (
	PiecesSpreadsheet = loadPiecesSpreadsheet()
	BoardSprite       = loadBoardSprite()
)

type (
	Spreadsheet struct {
		Picture pxl.Picture           // The spreadsheet data
		Sprites map[string]pxl.Sprite // Sprites cut out from the picture
	}

	boardSprite struct {
		pxl.Sprite
		// Width or height of the image, could be either
		Size float64
		// Hardcoded size of the tile (can't divide the sprite, there could be a border)
		Tilesize float64
	}
)

func PieceSprite(p Piece) pxl.Sprite {
	var reg, isDefined = PiecesSpreadsheet.Sprites[p.Notation()]
	Assert(isDefined, p)
	return reg
}

func loadBoardSprite() boardSprite {
	filename := "board.png"
	couldntLoad := func(err error) {
		if err != nil {
			Prt("Couldn't open the pieces spreadsheet file (%s).\n", filename)
			panic(err)
		}
	}

	var picture pxl.Picture
	{
		f, err := os.Open(filename)
		couldntLoad(err)
		defer f.Close()

		img, _, err := image.Decode(f)
		couldntLoad(err)

		picture = pxl.PictureDataFromImage(img)
	}

	spritePtr := pxl.NewSprite(picture, picture.Bounds())
	if spritePtr == nil {
		Prt("Couldn't make a sprite out of the board picture.\n")
		panic(nil)
	}
	return boardSprite{
		Sprite:   *spritePtr,
		Size:     picture.Bounds().W(),
		Tilesize: 39,
	}
}

func loadPiecesSpreadsheet() (s Spreadsheet) {

	// Loading the picture
	{
		filename := "pieces.png"
		couldntLoad := func(err error) {
			if err != nil {
				Prt("Couldn't open the pieces spreadsheet file (%s).\n", filename)
				panic(err)
			}
		}

		f, err := os.Open(filename)
		couldntLoad(err)
		defer f.Close()

		img, _, err := image.Decode(f)
		couldntLoad(err)

		s.Picture = pxl.PictureDataFromImage(img)
	}

	// Hardcoded regions
	{
		s.Sprites = make(map[string]pxl.Sprite)
		cellWidth := 32.0
		cell := pxl.R(0, 0,
			cellWidth,
			s.Picture.Bounds().H()) // 40?

		// Hardcoded list of what pieces these are, with their
		// notations. (see `Piece.Notation() string`)
		order := []string{
			// Warning: watch that king and promoted pieces!
			"K", ".K", "R", "B", "+R", "+B", "G", "S", "+S",
			"N", "+N", "L", "+L", "P", "+P",
		}
		for i := 0; ; i++ {
			if cell.Min.X > s.Picture.Bounds().W() {
				break // @infloop, should be 15 pieces
			}

			nameNotation := order[i]

			sprite := pxl.NewSprite(s.Picture, cell)
			if sprite == nil {
				Prt("Couln't create a sprite for %s\n", nameNotation)
				continue
			}
			s.Sprites[nameNotation] = *sprite

			// Move on to the next cell
			cell.Min.X += cellWidth
			cell.Max.X += cellWidth
		}
	}

	return s
}

func LoadPicture(filename string) (pxl.Picture, error) {

	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	img, _, err := image.Decode(f)
	if err != nil {
		return nil, err
	}

	return pxl.PictureDataFromImage(img), nil
}

//
//  Sounds
//

type soundAsset struct {
	beep.Buffer
	beep.Format
}

func (sa *soundAsset) Streamer() beep.Streamer {
	streamer := sa.Buffer.Streamer(0, sa.Buffer.Len())

	// Resample (timeshrink) to randomize the sounds a tiny bit
	speedup := 1 + (rand.Float64() * 0.2)
	sr := beep.SampleRate(float64(sa.SampleRate) * speedup)

	return beep.Resample(4, sa.SampleRate, sr, streamer)
}

var (
	Snd_Clack = "piece_clack.wav"

	loadedSFX = make(map[string]soundAsset)
)

func PlaySound(filename string) {
	s, loaded := loadedSFX[filename]
	if !loaded {
		mustLoadSound(filename)
	}
	speaker.Play(s.Streamer())
}

func init() { // load before app startup, why not.

	// Load in all wav files in the directory
	soundpaths, err := filepath.Glob("*.wav")
	canPanic(err)
	for _, path := range soundpaths {
		mustLoadSound(path)
	}

	// Initialize the speaker with some of the loaded sounds
	for _, sound := range loadedSFX {
		speaker.Init(
			sound.SampleRate,
			sound.SampleRate.N(time.Second/100),
		)
		break // Just take the first one's format, whatever.
	}
}

func mustLoadSound(filename string) {
	if !strings.HasSuffix(filename, ".wav") {
		panic("Only wav files are supported for loading")
	}

	f, err := os.Open(filename)
	canPanic(err)

	streamer, format, err := wav.Decode(f)
	canPanic(err)

	defer f.Close()
	defer streamer.Close()

	buf := *beep.NewBuffer(format)
	buf.Append(streamer)

	loadedSFX[filename] = soundAsset{
		Buffer: buf,
		Format: format,
	}
}
