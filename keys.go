package main

import gl "github.com/faiface/pixel/pixelgl"

var (
	// End the program with something a bit more work-environment-safe
	// than Alt+F4...    [A semicolon on my dvorak keyboard.]
	KEY_QUIT = gl.KeyZ

	KEY_LCLICK = gl.MouseButton1
	KEY_RCLICK = gl.MouseButton2
	KEY_MCLICK = gl.MouseButton3

	KEY_PAUSE = gl.KeyP
)
