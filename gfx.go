package main

import (
	pxl "github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"

	"math"
	"sort"
	"time"
)

// Transition constants for duration
const (
	Dur_NOW           SecsDT = 0.0 // To force a value without transitioning
	Dur_PieceMove     SecsDT = 0.3
	Dur_PieceTaken    SecsDT = 1.0
	Dur_BoardSpin     SecsDT = 0.4
	Dur_BoardSpinWait SecsDT = Dur_PieceMove + 0.2
)

// PieceHitboxes are rectangles in the window's coordinants
// to check a mouse pointer against.
// Piece hitboxes are defined by the matrix that we're using to draw the piece
// (i.e. the hitbox is defined in draw). This allows us to always have
// the correct hitbox for the piece, regardless of its position (both off
// and on the board).
var PieceHitboxes = make(map[*Piece]pxl.Rect)

// The same as PieceHitboxes but with tile positions.
var TileHitboxes = make(map[Pos]pxl.Rect)

var TurnNoticeAnim Notice
var ChoiceToPromote PromotionChoice

var PiecePreviewed *Piece

func EndTurn_gfx() {
	// NOTE: Call AFTER the change in data

	// @todo; Block input while flipping?

	// Set rotation animation props
	var rotTo float64
	if TheBoard.CurrentTurn == Player_UpsideDown {
		rotTo = math.Pi
	} else {
		rotTo = 0.0
	}

	// Start the rotation after a bit of time
	// (some of that golang magick! ...but the time setting is ugly)
	piecemoveWait := time.After(SecsToDuration(Dur_BoardSpinWait))
	go (func() {
		<-piecemoveWait
		StartTrans_Float(&TheBoard.Rotation, Dur_BoardSpin, rotTo)
	})()

	noticeWait := time.After(SecsToDuration(Dur_BoardSpinWait + 0.4))
	go (func() {
		<-noticeWait

		// Pop up a notice for the next turn
		tna := &TurnNoticeAnim
		StartTrans_Vec(&tna.Position, Dur_NOW, pxl.ZV)
		StartTrans_Vec(&tna.Position, 1.2, pxl.V(0, -10))

		var t = NewText(pxl.ZV)
		{
			line := Sprt("- %d -\n", TheBoard.Turn)
			t.Dot.X -= t.BoundsOf(line).W() / 2
			t.WriteString(line)
			var plName = "Black"
			if TheBoard.CurrentTurn == PlayerWhite {
				plName = "White"
			}
			line = Sprt("%s's turn", plName)
			t.Dot.X -= t.BoundsOf(line).W() / 2
			t.WriteString(line)
		}
		tna.text = t

		tna.Position.OnFinish = func() {
			tna.text = nil
		}
	})()
}

func Init() {
	InitDefaultPositions(&TheBoard)
	Prt(TheBoard.String())

	TheBoard.Rotation.Interp = interp_boardrot
}

func UpdateAnims(dt SecsDT) {

	// @todo; Create an array of all timers that we'll be batch-updating,
	//        the transition-runners can then have pointers to it.

	TurnNoticeAnim.Position.Update(dt) // Turn notice
	TheBoard.Rotation.Update(dt)       // Board rotation
	for i := range TheBoard.Pieces {   // Piece movement
		TheBoard.Pieces[i].DrawPos.Update(dt)
	}
}

func Draw() {
	var win = Window.Bounds()
	var winSize = math.Min(win.H(), win.W())

	var mx_TurnRotation, mx_CenterWindow pxl.Matrix
	var scale float64
	{
		var rot = TheBoard.Rotation.Value()

		scale = math.Max(1, math.Floor(winSize/BoardSprite.Size))
		var move = FloorV(win.Center())
		if int(scale)%2 == 0 {
			move = move.Add(HalfPX)
		}

		mx_TurnRotation = pxl.IM.Rotated(pxl.ZV, rot)
		mx_CenterWindow = pxl.IM.Moved(move)
	}
	Window.SetMatrix(mx_CenterWindow)

	{ // Draw the board
		var m = pxl.IM.Scaled(pxl.ZV, scale).Chained(mx_TurnRotation)
		if int(BoardSprite.Size)%2 != 0 { // Align board to pixel-perfect
			m = m.Moved(HalfPX)
		}
		BoardSprite.Draw(Window, m)
	}

	mx_posToCoord := pxl.IM.
		// Center on the tile itself
		Moved(pxl.V(-0.5, -0.5)).
		// Let's count positions from the center
		Moved(pxl.V(-4.5, -4.5)).
		// Blow up to the actual board sprite outwards from its centerx
		Scaled(pxl.ZV, BoardSprite.Tilesize*scale).
		// Rotate the coordinants with the board (doesn't rotate pieces)
		Chained(mx_TurnRotation)

	// Add the hitboxes for the board
	ts := BoardSprite.Tilesize
	for x := 1; x <= BoardTiles; x++ {
		for y := 1; y <= BoardTiles; y++ {
			pos := Pos{X: x, Y: y}
			coord := mx_posToCoord.Project(pos.Vec())

			rect := ExtentRect(pxl.R(0, 0, ts, ts))
			rect = ProjectRect(rect, pxl.IM.Scaled(pxl.ZV, scale))
			rect = OffsetRect(rect, coord)

			// Note: Tile hitbox isn't a rotated rectangle,
			// it's just repositioned. It's still a Rect, can't rotate that.

			TileHitboxes[pos] = ProjectRect(rect, mx_CenterWindow)
		}
	}

	// Draw all pieces, wherever they are
	for _, piece := range TheBoard.Pieces_ZSorted() {
		if piece == nil {
			continue // @todo; And get rid of the entry in the map?
		}

		sprite := PieceSprite(*piece)

		var xy = piece.DrawPos.Value()
		xy = mx_posToCoord.Project(xy)
		xy = FloorV(xy)

		var mx = pxl.IM.Scaled(pxl.ZV, scale).Chained(mx_TurnRotation)
		if piece.Loyalty == Player_UpsideDown {
			mx = mx.Rotated(pxl.ZV, math.Pi)
		}
		mx = mx.Moved(xy)

		// @bug; If the piece has an IM, it doesn't get drawn!
		if mx == pxl.IM {
			mx = pxl.IM.Moved(pxl.V(0.000001, 0))
		}
		if int(scale)%2 == 0 { // @artifacts
			mx = mx.Moved(HalfPX)
		}

		sprite.Draw(Window, mx)

		// @test Drawing some text next to the pieces
		// @todo; Use this to print number of same pieces in hand
		//        when stacking them.
		if false {
			t := NewText(pxl.ZV)
			t.WriteString(piece.Notation())
			offsetToPieceCorner := FloorV(pxl.Vec{
				X: sprite.Frame().W() * 0.4,
				Y: sprite.Frame().H()*0.5 - t.LineHeight,
			})
			t.Draw(Window, pxl.IM.
				Moved(offsetToPieceCorner).
				Chained(mx))
		}

		// Set the hitboxes from this draw
		hitboxScale := 0.8 * scale
		hitboxExtents := ExtentRectScaled(sprite.Frame(), hitboxScale)
		hitboxOnBoard := OffsetRect(hitboxExtents, xy)
		PieceHitboxes[piece] = ProjectRect(hitboxOnBoard, mx_CenterWindow)

		// Highlight the selected piece (use the hitbox for easier testing)
		// @todo; Better highlight effects, investigate pixel's mixing modes
		d := imdraw.New(nil)
		defer d.Draw(Window)
		if PieceSelected == piece {
			d.Color = COL_BLUE
			d.Push(hitboxOnBoard.Min, hitboxOnBoard.Max)
			d.Rectangle(0)
		}

		// Moves possible for the piece
		// @todo; Draw tile assets, rotated correctly etc.
		if piece == PiecePreviewed || piece == PieceSelected {
			d.Color = COL_RED
			if piece == PieceSelected {
				d.Color = COL_GREEN
			}
			rect := ExtentRect(pxl.R(0, 0, 10, 10))

			ms := ValidMovesFor(*piece)

			for _, pos := range ms {
				xy := mx_posToCoord.Project(pos.Vec())
				r := OffsetRect(rect, xy)

				d.Push(r.Min, r.Max)
				d.Rectangle(0)
			}
		}

	}

	{ // Draw text when the turn changes
		tna := &TurnNoticeAnim
		pos := tna.Position.Value()
		if tna.text != nil {
			center := tna.text.Bounds().Center()
			mx := pxl.IM.
				Moved(pos.Add(center.Scaled(-1))).
				Scaled(pxl.ZV, 2*scale)

			// Draw a background for the text to be readable
			d := imdraw.New(nil)
			padding := pxl.V(20, 5)
			backdrop := ProjectRect(pxl.Rect{
				Min: tna.text.Bounds().Min.Sub(padding),
				Max: tna.text.Bounds().Max.Add(padding),
			}, mx)

			var c = COL_BLACK
			{
				t := tna.Position.Factor()
				t = -((2*t - 1) * (2*t - 1)) + 1
				c.A = uint8(200 * t)
			}
			d.Color = c

			d.Push(backdrop.Min, backdrop.Max)
			d.Rectangle(0)
			d.Draw(Window)

			// Draw the text on top of the background
			c = COL_WHITE
			if true {
				t := tna.Position.Factor()
				t = -((2*t - 1) * (2*t - 1)) + 1
				f := uint8(255 * t)
				c.A, c.R, c.G, c.B = f, f, f, f
			}
			tna.text.DrawColorMask(Window, mx, c)
			// tna.text.Draw(Window, mx)
		}
	}
}

// Pieces_ZSorted returns a sorted slice of pointers to pieces
// both on and off the board.
// This list is sorted by most-recently-touched-piece last
// to aid in rendering. The last item in the list should
// be then rendered last to appear on the top.
func (b Board) Pieces_ZSorted() []*Piece {
	var allPieces = make([]*Piece, len(b.Pieces))
	for i := range b.Pieces {
		allPieces[i] = &b.Pieces[i]
	}
	sort.Slice(allPieces, func(i, j int) bool {
		return allPieces[i].lastMoved.Before(allPieces[j].lastMoved)
	})
	return allPieces
}

func CapturePiece_gfx(p *Piece) {
	// @todo; Make a positioning function based on hands of
	//        both players.
	var handDrawPos pxl.Vec
	if Player_PositionLow == PieceSelected.Loyalty {
		handDrawPos = pxl.V(float64(BoardTiles)+2, 1)
	} else {
		handDrawPos = pxl.V(-1, float64(BoardTiles))
	}
	StartTrans_Vec(&p.DrawPos, Dur_PieceTaken, handDrawPos)
}

func RenderAtTop(p *Piece) {
	p.lastMoved = time.Now()
}
