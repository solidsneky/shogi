#!/bin/sh

#
#   Building a Windows executable on Linux!
#

# Resources:
#  https://github.com/faiface/pixel/issues/53
#  https://www.glfw.org/docs/latest/compile.html#compile_deps_mingw_cross
#  https://stackoverflow.com/questions/1516609/difference-between-cc-gcc-and-g

# Target OS and processor architecture
# https://gist.github.com/asukakenji/f15ba7e588ac42795f421b48b8aede63
GOOS=windows
# Other CPU architectures might need some more tinkering.
# (And I'm not even talking about ARM for mobile or WebGL to run in the browser)
# GOARCH=x86  

# We'll have to compile some OpenGL, GLFW specifically.
# The source code for it is supposedly in GLFW, all that's left
# is using a C compiler that can do cross-platform builds.
CGO_ENABLED=1

# Requires the huge lib "mingw-w64" to be apt getted (1.1GB)
# with the compiler that can do windows-runnable .exe files.
# The parameter refers to the executable in `/usr/bin`.
# To list alternatives, run `ls /usr/bin | grep gcc`.
CC=x86_64-w64-mingw32-gcc  

# Finally, build the exe for windows.
# Note: Don't forget any assets or other files the executable
# may need to load at runtime.
go build -o shogi.exe
