module shogi

go 1.13

require (
	github.com/faiface/beep v1.0.2
	github.com/faiface/pixel v0.9.0
	golang.org/x/image v0.0.0-20190523035834-f03afa92d3ff
)
