package main

// Resources:
// https://en.wikipedia.org/wiki/Shogi

// Number of tiles on the board, same in X & Y
const BoardTiles int = 9

//
//  Movesets
//

func PromoPossible(fromTile_hasPiece Pos, toTile Pos) bool {
	var p, ok = PieceAt(fromTile_hasPiece)
	Assert(ok)

	if _, hasPromotion := Promotions[p.Type]; !hasPromotion {
		return false
	}

	var promoLanes []int
	if p.Loyalty == Player_PositionLow {
		promoLanes = []int{7, 8, 9}
	} else {
		promoLanes = []int{1, 2, 3}
	}

	for _, promoY := range promoLanes {
		// You can either be on the promotion lane or come from it,
		// both gives you the option to promote.
		if fromTile_hasPiece.Y == promoY || toTile.Y == promoY {
			return true
		}
	}
	return false
}

func Promote(p *Piece) {
	for demoted, promoted := range Promotions {
		if p.Type == demoted {
			p.Type = promoted
		}
	}
}
func Demote(p *Piece) {
	for demoted, promoted := range Promotions {
		if p.Type == promoted {
			p.Type = demoted
		}
	}
}

func DoMove(p *Piece, toTile Pos) (captured *Piece) {

	// Note: This function should always be preceded by checking the moveset
	//       to ensure the move is legal.

	// If we're dropping, the piece isn't captured anymore
	if p.Captured {
		Prt("Reviving piece :: %s", p)
		p.Captured = false
	}

	// Capture the piece that has been stepped on
	captured, _ = PieceAt(toTile)
	if captured != nil {
		CapturePiece(TheBoard.CurrentTurn, captured)
	}

	// Update the tile position
	p.TilePosition = toTile

	return
}

func ValidMovesFor(p Piece) []Pos {
	if !p.Captured {
		return moves(p)
	} else {
		return drops(p)
	}
}
func moves(p Piece) []Pos {
	if p.Captured {
		return nil // Check only valid moves, not drops
	}

	a, aP := p.TilePosition, p

	var moves = make([]Pos, 0)
	for _, m := range p.Moveset() {
		b := a.WithMove(m)
		if !isOnBoard(b) {
			continue // Cannot move outside the board
		}

		if isLongPathBlocked(a, b) {
			continue // Blocked long jumps (lances, rooks, bishops...)
		}

		bP, occupied := PieceAt(b)
		if occupied && bP.Loyalty == aP.Loyalty {
			continue // Stepping on my own piece
		}

		moves = append(moves, b)
	}
	return moves
}
func drops(p Piece) []Pos {
	if p.Captured == false {
		return nil // Only captured pieces can drop
	}

	var dr = make([]Pos, 0) // Possible drop positions
	var boardspan = []int{1, 2, 3, 4, 5, 6, 7, 8, 9}

	for _, x := range boardspan {

		// Test if the column has a pawn with the same loyalty already,
		// in which case don't bother checking anything else (two pawns rule).
		var droppingPawn = (p.Type == T_Pawn)
		var skipColumn = false
		if droppingPawn {
			for _, y := range boardspan {
				var (
					pos             = Pos{X: x, Y: y}
					piece, tileFull = PieceAt(pos)
				)
				if tileFull {
					var pieceIsPawn = (piece.Type == T_Pawn)
					var pieceIsMine = (piece.Loyalty == p.Loyalty)
					if pieceIsPawn && pieceIsMine {
						skipColumn = true
						continue // Skip checking for this column
					}
				}
			}
		}
		if skipColumn {
			continue
		}

		for _, y := range boardspan {
			var (
				pos           = Pos{X: x, Y: y}
				_, isOccupied = PieceAt(pos)
			)
			if !isOccupied {
				var copy = p            // Test with a copy
				copy.Captured = false   // Reset to allow move checks
				copy.TilePosition = pos // Set to tested position

				movesIfDroppedThere := moves(copy)
				if len(movesIfDroppedThere) != 0 {
					dr = append(dr, pos)
				}
			}
		}
	}

	// @todo; Checkmate test as a filter

	return dr
}
func isLongPathBlocked(a, b Pos) bool {
	const (
		BLOCKED     = true
		NOT_BLOCKED = false
	)

	var (
		diagonal    = AbsInt(a.X-b.X) == AbsInt(a.Y-b.Y)
		lineXorY    = (a.X == b.X) || (a.Y == b.Y)
		isBlockable = diagonal || lineXorY
	)
	if !isBlockable {
		return NOT_BLOCKED
	}

	// If we go from `a` to `b` by Sign() increments,
	// we should always get to `b`, gived it's either diagonal
	// (sx = 1/-1 , sy = 1/-1) or on a horizontal or vertical line
	// (sx or sy is zero, incrementing one towards the right side).
	//
	// A pretty good solution instead of a complex array building.

	signX := SignInt(b.X - a.X)
	signY := SignInt(b.Y - a.Y)
	var pos = a

	for {
		// Changing at the start, don't check the `a` piece itself.
		pos = Pos{X: pos.X + signX, Y: pos.Y + signY}

		// Check for destination before checking if occupied,
		// the goal is to get there. Of course `b` blocks it, that's
		// the point.
		if pos == b {
			// We got to point `b` without anyone blocking us
			return NOT_BLOCKED
		}

		// We're not in `b` yet, we have to check if the tile's empty.
		if _, occupied := PieceAt(pos); occupied {
			// Can't move to this point, the path to it is blocked
			return BLOCKED
		}
	}
}

func PieceAt(tilePos Pos) (*Piece, bool) {
	Assert(isOnBoard(tilePos))

	var occupied *Piece = nil
	for i := range TheBoard.Pieces {
		if TheBoard.Pieces[i].Captured {
			continue // Piece is not on TheBoardoard, the position is just leftover
		}
		if TheBoard.Pieces[i].TilePosition == tilePos {
			occupied = &TheBoard.Pieces[i]
		}
	}
	return occupied, occupied != nil
}
func isOnBoard(a Pos) bool {
	var xok = (a.X >= 1 && a.X <= BoardTiles)
	var yok = (a.Y >= 1 && a.Y <= BoardTiles)
	return xok && yok
}

func (p Piece) Moveset() []PosD {
	m, defined := Movesets[p.Type]
	if !defined {
		panic(Sprt("Undefined moveset :: %d", p.Type))
	}

	// Flip the moveset for the white player (careful to copy not modify)
	if p.Loyalty == Player_UpsideDown {
		mFlipped := make([]PosD, len(m))
		for i := range m {
			mFlipped[i] = m[i]      // Copy move
			mFlipped[i].Y = -m[i].Y // Flip y
		}
		m = mFlipped
	}

	return m
}

func CapturePiece(pl Player, p *Piece) {

	// Demote the piece added to the hand
	for demoted, promoted := range Promotions {
		if p.Type == promoted {
			p.Type = demoted
		}
	}

	p.Captured = true
	p.Loyalty = pl
}

func EndTurn() {
	TheBoard.CurrentTurn = !TheBoard.CurrentTurn
	TheBoard.Turn += 1
}

//
//  Initial positions for pieces
//

func InitDefaultPositions(b *Board) {
	var pos Pos                          // Think of it as a cursor...
	var addPiece = func(typ PieceType) { // @closure(pos)
		var loyalty Player // Loyalty inferred from the side on the board
		if pos.Y < 5 {
			loyalty = PlayerBlack
		} else {
			loyalty = PlayerWhite
		}

		p := Piece{
			TilePosition: pos,
			Type:         typ,
			Loyalty:      loyalty,
			Captured:     false,
		}
		p.forceDrawPosToCurrent() // Force the drawn position
		p.DrawPos.OnFinish = func() { PlaySound(Snd_Clack) }
		p.DrawPos.Interp = interp_piecemove
		b.Pieces = append(b.Pieces, p) // Register piece onto board
	}

	if false { // @test; Get something into the hand immediatelly
		defer func() {
			p := Piece{
				Type:         T_Pawn,
				Loyalty:      PlayerBlack,
				TilePosition: Pos{X: -1, Y: 1},
				Captured:     true,
			}
			p.forceDrawPosToCurrent()
			p.DrawPos.OnFinish = func() { PlaySound(Snd_Clack) }
			p.DrawPos.Interp = interp_piecemove

			b.Pieces = append(b.Pieces, p)
		}()
	}

	{ // Very manual position init
		pos.Y = 3
		for pos.X = 1; pos.X <= BoardTiles; pos.X++ {
			addPiece(T_Pawn)
		}
		pos.Y = 7
		for pos.X = 1; pos.X <= BoardTiles; pos.X++ {
			addPiece(T_Pawn)
		}

		pos.Y = 2 // Note: Bishop faces the opponent's rook & vice versa
		{
			pos.X = 2
			addPiece(T_Bishop)
			pos.X = 8
			addPiece(T_Rook)
		}

		pos.Y = 8
		{
			pos.X = 2
			addPiece(T_Rook)
			pos.X = 8
			addPiece(T_Bishop)
		}

		pos.Y = 1
		{
			pos.X = 5
			addPiece(T_King)
			pos.X = 6
			addPiece(T_GenGold)
			pos.X = 4
			addPiece(T_GenGold)
			pos.X = 7
			addPiece(T_GenSilver)
			pos.X = 3
			addPiece(T_GenSilver)
			pos.X = 8
			addPiece(T_Knight)
			pos.X = 2
			addPiece(T_Knight)
			pos.X = 9
			addPiece(T_Lance)
			pos.X = 1
			addPiece(T_Lance)
		}

		pos.Y = 9
		{
			pos.X = 5
			addPiece(T_King)
			pos.X = 6
			addPiece(T_GenGold)
			pos.X = 4
			addPiece(T_GenGold)
			pos.X = 7
			addPiece(T_GenSilver)
			pos.X = 3
			addPiece(T_GenSilver)
			pos.X = 8
			addPiece(T_Knight)
			pos.X = 2
			addPiece(T_Knight)
			pos.X = 9
			addPiece(T_Lance)
			pos.X = 1
			addPiece(T_Lance)
		}
	}
}
func (p *Piece) forceDrawPosToCurrent() {
	var popped = p.DrawPos.OnFinish // Force reposition without sounds
	p.DrawPos.OnFinish = nil

	StartTrans_Vec(&p.DrawPos, Dur_NOW, p.TilePosition.Vec())

	p.DrawPos.OnFinish = popped
}

//
//  Piece data and moveset definition
//

var Notations = (func() map[PieceType]string {
	m := map[PieceType]string{
		T_King:         "K",
		T_Rook:         "R",
		T_Rook_Pr:      "+R",
		T_Bishop:       "B",
		T_Bishop_Pr:    "+B",
		T_GenGold:      "G",
		T_GenSilver:    "S",
		T_GenSilver_Pr: "+S",
		T_Knight:       "N",
		T_Knight_Pr:    "+N",
		T_Lance:        "L",
		T_Lance_Pr:     "+L",
		T_Pawn:         "P",
		T_Pawn_Pr:      "+P",
		T_MaxValue:     ".",
	}
	// Assert missing
	for pi := PieceType(0); pi <= T_MaxValue; pi++ {
		if _, defined := m[pi]; !defined {
			panic(Sprt("Undefined notation :: %d", pi))
		}
	}
	return m
})()
var Promotions = map[PieceType]PieceType{
	T_Rook:      T_Rook_Pr,
	T_Bishop:    T_Bishop_Pr,
	T_GenSilver: T_GenSilver_Pr,
	T_Knight:    T_Knight_Pr,
	T_Lance:     T_Lance_Pr,
	T_Pawn:      T_Pawn_Pr,
}
var Movesets = (func() map[PieceType][]PosD {
	m := make(map[PieceType][]PosD)

	m[T_Pawn] = []PosD{{Y: +1}}

	m[T_Lance] = (func() (ms []PosD) { // straight up
		for i := 1; i <= 9; i++ {
			ms = append(ms, PosD{Y: i})
		}
		return
	})()

	m[T_Rook] = (func() (ms []PosD) { // + cross
		for i := 1; i <= 9; i++ {
			ms = append(ms,
				PosD{X: +i},
				PosD{X: -i},
				PosD{Y: +i},
				PosD{Y: -i},
			)
		}
		return
	})()

	m[T_Bishop] = (func() (ms []PosD) { // x cross
		for i := 1; i <= 9; i++ {
			ms = append(ms,
				PosD{X: +i, Y: +i},
				PosD{X: -i, Y: +i},
				PosD{X: +i, Y: -i},
				PosD{X: -i, Y: -i},
			)
		}
		return
	})()

	m[T_GenGold] = []PosD{
		{X: +1}, {X: -1}, // to the sides
		{X: 0, Y: +1}, // front row
		{X: +1, Y: +1},
		{X: -1, Y: +1},
		{Y: -1}, // back
	}

	m[T_GenSilver] = []PosD{
		{X: 0, Y: +1}, // front row
		{X: +1, Y: +1},
		{X: -1, Y: +1},
		{X: -1, Y: -1}, {X: +1, Y: -1}, // backdiag
	}

	m[T_Knight] = []PosD{
		{X: +1, Y: +2}, {X: -1, Y: +2}, // chesshorse forw
	}

	m[T_King] = appendMoves(m[T_GenSilver], m[T_GenGold]...) // hack!

	// Promoted ........................................

	// Added king moves around the piece
	m[T_Rook_Pr] = appendMoves(m[T_Rook], m[T_King]...)
	m[T_Bishop_Pr] = appendMoves(m[T_Bishop], m[T_King]...)
	// Same moveset as the gold general
	for _, p := range []PieceType{
		T_GenSilver_Pr, T_Knight_Pr, T_Lance_Pr, T_Pawn_Pr,
	} {
		m[p] = m[T_GenGold]
	}

	// Assert if any are missing .......................
	for pi := PieceType(0); pi < T_MaxValue; pi++ {
		if _, defined := m[pi]; !defined {
			panic(Sprt("Undefined moveset :: %d", pi))
		}
	}

	return m
})()

func appendMoves(moves []PosD, options ...PosD) []PosD {
	if moves == nil {
		panic(nil)
	}

	// Avoid inserting duplicates
	var extra = make([]PosD, 0)
	for _, new := range options {
		var missing = true
		for _, has := range moves {
			if has.X == new.X && has.Y == new.Y {
				missing = false
			}
		}
		if missing {
			extra = append(extra, new)
		}
	}

	return append(moves, extra...)
}
