package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"image/color"
	"math"
	"strings"
	"time"

	pxl "github.com/faiface/pixel"
	gl "github.com/faiface/pixel/pixelgl"
)

// All colors are to be mentioned here for avoiding the color import
var (
	// Basic colors, with transparency for more debugging power
	COL_WHITE = color.RGBA{255, 255, 255, 100}
	COL_BLACK = color.RGBA{0, 0, 0, 100}
	COL_RED   = color.RGBA{168, 30, 30, 100}
	COL_GREEN = color.RGBA{30, 168, 30, 100}
	COL_BLUE  = color.RGBA{30, 30, 168, 100}

	// Background color for the board
	COL_BOARD = color.RGBA{120, 29, 79, 255}

	COL_OPAQUE = color.RGBA{0, 0, 0, 200}

	// Wiping color
	COL_NULL = color.RGBA{0, 0, 0, 0}
)

//
//  ==== controls layer away from pixelgl ====
//

func (b Board) String() string {
	var sb strings.Builder
	sb.WriteString("+---+---+---+---+---+---+---+---+---+\n")
	for y := 1; y <= BoardTiles; y++ {
		sb.WriteString("|")
		for x := 1; x <= BoardTiles; x++ {
			tile, occupied := PieceAt(Pos{x, y})
			if !occupied {
				sb.WriteString("   ")
			} else {
				not := tile.Notation()
				if len(not) != 2 {
					sb.WriteString(" ")
				}
				sb.WriteString(not)
				if tile.Loyalty == Player_UpsideDown {
					sb.WriteString(")")
				} else {
					sb.WriteString(" ")
				}
			}
			sb.WriteString("|")
		}
		sb.WriteString("\n+---+---+---+---+---+---+---+---+---+\n")
	}
	return sb.String()
}

func (p Piece) String() string {
	var loy string
	if p.Loyalty == PlayerWhite {
		loy = "w"
	} else {
		loy = "b"
	}
	capped := ""
	if p.Captured {
		capped = " CAPTURED"
	}

	return Sprt("%s(%s) [%d:%d]%s",
		p.Notation(), loy,
		p.TilePosition.X, p.TilePosition.Y, capped)
}

func (p Piece) Notation() string {
	name, defined := Notations[p.Type]
	if !defined {
		panic(Sprt("Undefined notation :: %d", p.Type))
	}

	// King has a different graphic on the black's side.
	// With this, the notation can be used to keep track of
	// the sides.
	if p.Type == T_King && p.Loyalty == PlayerBlack {
		name = "." + name // suffix
	}

	return name
}

// Toggle the main game window's fullscreen
func ToggleFullscreen() {
	Window.SetMonitor(gl.PrimaryMonitor())
}

func AbsInt(a int) int {
	if a > 0 {
		return -a
	}
	return a
}

// Unpack the x and y from a pixel.Vec
func Unpack(v pxl.Vec) (float64, float64) {
	return v.X, v.Y
}

// Halfpixels, mostly used for re-aligning sprite drawing to avoid
// graphical artifacts.
var HalfPX = pxl.Vec{X: 0.5, Y: 0.5}
var HalfPXneg = pxl.Vec{X: -0.5, Y: -0.5}

// Align drawing to the center of the pixles. IMdrawing is done by
// centering lines around the points/lines, so we should always
// adjust it by 0.5 in both X and Y to call the draw on the pixel's
// dead center, and have the thickness of 1px fill the pixel itself.
func PixelAlignedVec(v pxl.Vec) pxl.Vec {
	return v.Add(HalfPX)
}

func FloorV(v pxl.Vec) pxl.Vec {
	return pxl.Vec{
		X: math.Floor(v.X),
		Y: math.Floor(v.Y),
	}
}

// ExtentRect returns a centered rectangle, with min.x == -max.x
// and so on. It can be used to extract extents from a sprite frame
// to know the dimensions of the expected sprite.
func ExtentRect(r pxl.Rect) pxl.Rect {
	return pxl.Rect{
		Min: pxl.V(-r.W()/2, -r.H()/2),
		Max: pxl.V(+r.W()/2, +r.H()/2),
	}
}
func ExtentRectScaledXY(r pxl.Rect, x, y float64) pxl.Rect {
	ex := ExtentRect(r)
	return pxl.Rect{
		Min: pxl.V(ex.Min.X*x, ex.Min.Y*y),
		Max: pxl.V(ex.Max.X*x, ex.Max.Y*y),
	}
}
func ExtentRectScaled(r pxl.Rect, s float64) pxl.Rect {
	return ExtentRectScaledXY(r, s, s)
}

// OffsetRect offsets a rectangle by a vector in both X and Y
func OffsetRect(r pxl.Rect, o pxl.Vec) pxl.Rect {
	return pxl.Rect{
		Min: r.Min.Add(o),
		Max: r.Max.Add(o),
	}
}
func OffsetRectXY(r pxl.Rect, x float64, y float64) pxl.Rect {
	return OffsetRect(r, pxl.V(x, y))
}
func ProjectRect(r pxl.Rect, m pxl.Matrix) pxl.Rect {
	return pxl.Rect{
		Min: m.Project(r.Min),
		Max: m.Project(r.Max),
	}
}
func UnprojectRect(r pxl.Rect, m pxl.Matrix) pxl.Rect {
	return pxl.Rect{
		Min: m.Unproject(r.Min),
		Max: m.Unproject(r.Max),
	}
}

func (p Pos) Vec() pxl.Vec {
	return pxl.Vec{X: float64(p.X), Y: float64(p.Y)}
}
func (p PosD) Vec() pxl.Vec {
	return pxl.Vec{X: float64(p.X), Y: float64(p.Y)}
}
func (p Pos) WithMove(d PosD) Pos {
	return Pos{X: p.X + d.X, Y: p.Y + d.Y}
}

// Go doesn't have a sign func in "math" because the import would be
// too big for something as trivial as a sign().
func Sign(n float64) float64 {
	if n < 0 {
		return -1
	} else if n > 0 {
		return 1
	}
	return 0
}
func SignInt(n int) int {
	if n < 0 {
		return -1
	} else if n > 0 {
		return 1
	}
	return 0
}

// Matrix decomposition to scale and translations,
// probably won't work on matrices that are using more than just
// scaling and translations.
// @todo; Detect and warn when the function is used on a matrix
//        with rotation or skewing (needs testing the diagonals).
func DecomposeMatrix(c pxl.Matrix) (
	scale float64, translationAbsolute, translationRelative pxl.Vec,
) {
	testA := pxl.V(0, 0)
	testB := pxl.V(100, 100)
	ABdiff := testA.Sub(testB)
	ABdiff2 := c.Unproject(testA).Sub(c.Unproject(testB))

	scaleXY := pxl.Vec{
		X: ABdiff.X / ABdiff2.X,
		Y: ABdiff.Y / ABdiff2.Y}
	if scaleXY.X-scaleXY.Y > 0.00001 { // Could be the ol' float imprecision
		Prt("\nWARNING:\n Scale is not the same in X and Y!\n")
	}

	scale = scaleXY.X // Shouldn't matter which
	translationAbsolute = c.Unproject(testA)
	translationRelative = translationAbsolute.Scaled(scaleXY.X)
	return
}
func DecomposeMatrixInverse(c pxl.Matrix) (
	scale float64, translationAbsolute, translationRelative pxl.Vec,
) {
	scale, transAbs, transRel := DecomposeMatrix(c)
	return 1 / scale, transAbs.Scaled(-1), transRel.Scaled(-1)
}

//
//  ==== debug tooling ====
//

func Prt(format string, stuff ...interface{}) {
	fmt.Printf(format, stuff...)
}
func PrtNow(thing interface{}) {
	fmt.Printf("%v\n", thing)
}
func PrtSep() {
	fmt.Printf("-----------------\n")
}
func Sprt(format string, stuff ...interface{}) string {
	return fmt.Sprintf(format, stuff...)
}

func PrtJSON(thing interface{}) {
	bytes, err := json.MarshalIndent(thing, " | ", "  ")
	if err != nil {
		fmt.Println("Debug print failed somewhere")
	}
	fmt.Println(string(bytes))
}
func PrtNamedJSON(name string, thing interface{}) {
	bytes, err := json.MarshalIndent(thing, name+": | ", "  ")
	if err != nil {
		fmt.Println("Debug print failed somewhere")
	}
	fmt.Println(string(bytes))
}

//
//  ==== errors ====
//

func canPanic(err error) {
	if err != nil {
		panic(err)
	}
}

var _assert_error = errors.New("Assertion error")

func Assert(must_pass_condition bool, printables ...interface{}) {
	if must_pass_condition == false {
		Prt("\n") // For readability

		if len(printables) > 0 { // Print the passed context printables
			Prt("Assertion context: \n")
			for _, p := range printables {
				Prt("  %#v\n", p)
			}
			Prt("\n")
		}

		panic(_assert_error)
	}
}

//
//  ==== time utilities ====
//

// To make sure everybody is at the same page about what `dt` is,
// here is the conversion function. This way I won't have to import
// the "time" package for every Update(dt) function I need to make.
func DurationToDt(d time.Duration) SecsDT {
	return d.Seconds()
}
func SecsToDuration(dt SecsDT) time.Duration {
	return time.Duration(dt * 1_000_000_000) // Secs to Nanos
}

//
//  ==== animation and transitions ====
//

func lerp(a, b, t float64) float64 {
	return a + ((b - a) * t)
}
func interp_piecemove(t float64) float64 {
	// Slow start, sudden end
	// (piece click works well with it)
	return t * t
}
func interp_boardrot(x float64) float64 {
	// Approximate an ease-in-out sine
	var y float64
	if x < 0.5 {
		y = (x * x * 2)
	} else {
		y = -((x - 1) * (x - 1) * 2) + 1
	}
	// Cut the tail short, rotations have really obvious artifacts
	y = y * 1.05
	if y > 1 {
		return 1.0
	} else {
		return y
	}
}

func StartTrans_Float(fl *TransitionFloat, length SecsDT, valEnd float64) {
	if length <= 0.0 {
		// Without transition, but still keep valid state;
		// there could be some problems with not doing it this way.
		StartTrans_Float(fl, 0.000000000001, valEnd)
		fl.Update(0.00000001)
		return
	}

	fl.valStart = fl.Value() // Go from the value we are at now.
	fl.valEnd = valEnd       // End value is explicitly set.

	ttlPositive := math.Max(0, fl.ttl)
	fl.ttl = length - ttlPositive // In case there's still a transition,
	fl.ttlStart = fl.ttl          // don't start it from zero.
}
func (fl *TransitionFloat) Update(dt SecsDT) {
	if fl.ttl >= 0.0 {
		fl.ttl -= dt // TTL counts down towards zero.

		// We JUST went over the zero, there won't be a next update.
		if fl.ttl < 0.0 && fl.OnFinishCallback != nil {
			fl.OnFinishCallback()
		}
	}
}
func (fl TransitionFloat) Value() float64 {
	if fl.ttlStart <= 0 {
		return fl.valStart // Is it a good fallback?
	}
	t := fl.Factor()
	a := fl.valStart
	b := fl.valEnd

	var value float64
	if fl.Interp == nil {
		value = lerp(a, b, t)
	} else {
		value = a + (b-a)*fl.Interp(t)
	}
	return value
}

func (fl TransitionFloat) Factor() float64 {
	t := 1 - (fl.ttl / fl.ttlStart)
	t = pxl.Clamp(t, 0, 1)
	return t
}

func StartTrans_Vec(tv *TransitionVec, length SecsDT, valEnd pxl.Vec) {
	StartTrans_Float(&tv.xt, length, valEnd.X)
	StartTrans_Float(&tv.yt, length, valEnd.Y)
}
func (tv *TransitionVec) Update(dt SecsDT) {
	tv.xt.Interp = tv.Interp // Dunno where else to do this...
	tv.yt.Interp = tv.Interp

	ttlBefore := tv.xt.ttl // Shouldn't matter which we pick
	tv.xt.Update(dt)
	tv.yt.Update(dt)
	ttlAfter := tv.xt.ttl

	if ttlBefore >= 0.0 && ttlAfter < 0 && tv.OnFinish != nil {
		tv.OnFinish()
	}
}
func (tv TransitionVec) Value() pxl.Vec {
	return pxl.Vec{
		X: tv.xt.Value(),
		Y: tv.yt.Value(),
	}
}
func (tv TransitionVec) Factor() float64 {
	return tv.xt.Factor()
}
