package main

import (
	pxl "github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"

	"math"
)

type (
	menuItem struct {
		Hitbox  pxl.Rect
		OnClick func()
	}
	menuAsset struct {
		Vec      pxl.Vec
		Sprite   pxl.Sprite
		Filename string
	}
)

var (
	// Clickable things
	MenuLines = []menuItem{
		{
			OnClick: Line_StartGame,
			Hitbox:  pxl.R(205, -150, 288, -180),
		}, {
			OnClick: Line_OpenOptions,
			Hitbox:  pxl.R(205, -150-34, 288+48, -180-34),
		}, {
			OnClick: Line_Exit,
			Hitbox:  pxl.R(205, -150-34-34, 288-10, -180-34-34),
		},
	}
	MenuItemIndex = 0

	// Assets
	Front      = menuAsset{Filename: "menu_front.png"}
	Background = menuAsset{Filename: "menu_back.png"}
	AllAssets  = []*menuAsset{&Background, &Front} // Render order
)

// Menu Actions
func ToggleMenu() {
	MenuVisible = !MenuVisible
	MenuItemIndex = 0

	PlaySound(Snd_Clack)
}
func Line_StartGame() {
	InitDefaultPositions(&TheBoard)
	ToggleMenu()
}
func Line_OpenOptions() {

}
func Line_Exit() {
	// @todo; A goodbye would be nice.
	Window.SetClosed(true)
}

func InitMenu() {
	// Asset loading
	for i := range AllAssets {
		picture, err := LoadPicture(AllAssets[i].Filename)
		if err != nil {
			Prt("Couldn't load main menu asset `%s`\n", AllAssets[i].Filename)
			panic(err)
		}
		spritePtr := pxl.NewSprite(picture, picture.Bounds())
		AllAssets[i].Sprite = *spritePtr
	}
}

func MenuFrame(dt SecsDT) {
	win := Window.Bounds()

	// Let's draw everything from the center.
	mx_centerWindow := pxl.IM.Moved(win.Center())
	Window.SetMatrix(mx_centerWindow)

	// Find out if we need to scale down the assets.
	var scale float64
	{
		// Assume the background's the biggest thing on the screen...
		menu := Background.Sprite.Frame()
		menuSize := math.Min(menu.H(), menu.W())
		winSize := math.Min(win.H(), win.W())
		scale = float64(int(2*(winSize/menuSize))) / 2.0
	}
	mx_scale := pxl.IM.Scaled(pxl.ZV, scale)

	for _, ass := range AllAssets {
		ass.Sprite.Draw(Window, mx_scale.Moved(ass.Vec))
	}

	// @test Draw the hitboxes for clicking
	d := imdraw.New(nil)
	for _, line := range MenuLines {

		r_screen := line.Hitbox
		r_screen = ProjectRect(r_screen, mx_scale)
		r_screen = ProjectRect(r_screen, mx_centerWindow).Norm()
		r_screen = r_screen.Norm() // otherwise .Contains() is effed apparently

		if r_screen.Contains(Window.MousePosition()) {
			d.Color = COL_BLUE
			if Window.JustPressed(KEY_LCLICK) {
				line.OnClick()
			}
		} else {
			d.Color = COL_GREEN
		}

		r := ProjectRect(line.Hitbox, mx_scale)
		d.Push(r.Min, r.Max)
		d.Rectangle(0)
	}
	d.Draw(Window)
}
